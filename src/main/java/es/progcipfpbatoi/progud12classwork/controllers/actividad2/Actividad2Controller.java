package es.progcipfpbatoi.progud12classwork.controllers.actividad2;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import es.progcipfpbatoi.progud12classwork.services.MySQLConnection;

@Controller
public class Actividad2Controller {

	@Autowired
	private MySQLConnection mySQLConnection; 
	
	@GetMapping("/actividad2")
	@ResponseBody
	public String consultasActividad2() {
		
		Connection connection = mySQLConnection.getConnection();
		
		try (Statement statement = connection.createStatement();){
		
			StringBuilder respuesta = new StringBuilder();
			
			appendCodigoYDescripcionDeTareas(statement, respuesta);
			appendNombresCategorias(statement, respuesta);
			appendDescripcionFechaYRealizada(statement, respuesta);
			
			return respuesta.toString();
        } catch (SQLException e) {
            return e.getMessage();
        }
	}
	
	private static void appendCodigoYDescripcionDeTareas(Statement statement, StringBuilder respuesta) throws SQLException {
		respuesta.append("<p>-----1 . Código y descripción de las tareas---------</p>");
		
		String sql = "SELECT codigo, descripcion FROM tareas";
		try (ResultSet rs = statement.executeQuery(sql);) {
			while (rs.next()) {
				respuesta.append("<p>Código: " + rs.getInt("codigo"));
				respuesta.append(" Descripción: " + rs.getString("descripcion") + "</p>");
			}
		}
	}
	
	private static void appendNombresCategorias(Statement statement, StringBuilder respuesta) throws SQLException {
		
		respuesta.append("<p>-----2 . Nombres de las categorías ---------</p>");
		
		String sql = "SELECT nombre FROM categorias";
		try (ResultSet rs = statement.executeQuery(sql);) {
		
			while (rs.next()) {
				respuesta.append("<p>Nombre: " + rs.getString("nombre") + "</p>");
			}
		}
	}
	
	private static void appendDescripcionFechaYRealizada(Statement statement, StringBuilder respuesta) throws SQLException {
		
		respuesta.append("<p>-----3. Descripción, fecha de creación y si ha sido realizada la tarea de aquellas de categoría 1 o 2 ---------</p>");
		
		String sql = "SELECT descripcion, fechaCreacion, realizada FROM tareas";
		try (ResultSet rs = statement.executeQuery(sql);) {
		
			while (rs.next()) {
				respuesta.append("<p>Descripcion: " + rs.getString("descripcion"));
				LocalDateTime fechaCreacion = rs.getTimestamp("fechaCreacion").toLocalDateTime();
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
				respuesta.append(" Fecha creación: " + fechaCreacion.format(formatter));
				respuesta.append(" Realizada: " + (rs.getInt("realizada") == 0?"No":"Sí") + "</p>");
			}
		}
	}

}
