package es.progcipfpbatoi.progud12classwork.controllers.actividad4;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import es.progcipfpbatoi.progud12classwork.services.MySQLConnection;

@Controller
public class Actividad4Controller {

	@Autowired
	private MySQLConnection mySQLConnection; 
	
	@GetMapping("/tarea-default-add-ps")
	@ResponseBody
	public String insertaTareaPorDefecto(
			@RequestParam String descripcion,
			@RequestParam String usuario) {
		
		String sql = String.format(""
				+ "INSERT INTO tareas "
				+ "(usuario, descripcion, fechaCreacion, "
				+ "vencimiento, prioridad, realizada, categoria_id) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?)");
		
		Connection connection = mySQLConnection.getConnection();
		
		try (PreparedStatement ps = connection.prepareStatement(sql)) {
			
			ps.setString(1, usuario);
			ps.setString(2, descripcion);
			ps.setTimestamp(3, Timestamp.valueOf(LocalDateTime.now()));
			ps.setTimestamp(4, Timestamp.valueOf(LocalDateTime.now().plusDays(2)));
			ps.setString(5, "BAJA");
			ps.setInt(6, 0);
			ps.setNull(7, Types.INTEGER);
			
			int rowsAffected = ps.executeUpdate();
			return rowsAffected + " nueva tarea insertada";
			
		} catch(SQLException ex) {
			return "Ha habido un error:" + ex.getMessage();
		}
		
		
	}

}
