package es.progcipfpbatoi.progud12classwork.controllers.examples;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import es.progcipfpbatoi.progud12classwork.services.MySQLConnection;

@Controller
public class Example5Controller {

	@Autowired
	private MySQLConnection mySQLConnection; 
	
	@GetMapping("/example5")
	@ResponseBody
	public String example5InsertWithPreparedStatementAndNewKeyInfo() {
		
		Connection connection = mySQLConnection.getConnection();
		
		String sql = "INSERT INTO categorias (nombre,icono, descripcion) VALUES (?,?,?)";
        
		try (PreparedStatement preparedStatement = connection.prepareStatement(
										sql, PreparedStatement.RETURN_GENERATED_KEYS)) {
					
			preparedStatement.setString (1, "Admnistrativas");
			preparedStatement.setString (2, "admin.png");
			preparedStatement.setString (3, "Tareas como renovar el dni, presentar la renta...");
			
			int affectedRows = preparedStatement.executeUpdate();
			
			try (ResultSet rs = preparedStatement.getGeneratedKeys()) {
				if (rs.next()) {
					int autoIncremental = rs.getInt(1);
					StringBuilder respuesta = new StringBuilder();
					
					respuesta.append("Se ha creado " + affectedRows 
							+ " nueva tarea con id " + autoIncremental);
					
					return respuesta.toString();
				}
			}
			
			return "No se ha creado una nueva categoría";
			
        } catch (SQLException e) {
            return e.getMessage();
        }
	}
}


