package es.progcipfpbatoi.progud12classwork.controllers.examples;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import es.progcipfpbatoi.progud12classwork.services.MySQLConnection;

@Controller
public class Example2Controller {
	
	@Autowired
	private MySQLConnection mySQLConnection; 
	
	@GetMapping("/example2")
	@ResponseBody
	public String selectExample() {
		Connection connection = mySQLConnection.getConnection();
		
		String sql = "SELECT descripcion, realizada FROM tareas WHERE usuario = 'Juan'";
		
		try (
			 Statement statement = connection.createStatement();
		     ResultSet rs = statement.executeQuery(sql);
		    ){
			
			StringBuilder respuesta = new StringBuilder();
			
			while (rs.next()) {
				respuesta.append("<p>------------------------</p>");
				respuesta.append("<p>Descripción: " + rs.getString("descripcion"));
				respuesta.append("<br/>Realizada: " + 
							(rs.getInt("realizada") == 0?"No":"Sí") + "</p>");
			}
			return respuesta.toString();
        } catch (SQLException e) {
            return e.getMessage();
        }
	}
}

