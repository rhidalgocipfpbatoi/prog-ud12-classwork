package es.progcipfpbatoi.progud12classwork.controllers.examples;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import es.progcipfpbatoi.progud12classwork.services.MySQLConnection;

@Controller
public class Example3Controller {
	
	@Autowired
	private MySQLConnection mySQLConnection; 
	
	@GetMapping("/example3")
	@ResponseBody
	public String exampleInsertNewCategory() {
		String sql = "INSERT INTO categorias (id, nombre, icono) "
				+ "VALUES (6, 'Trabajo', 'work.png')";

		Connection connection = mySQLConnection.getConnection();		
		try (Statement statement = connection.createStatement())
		{
			int affectedRows = statement.executeUpdate(sql);
			return affectedRows + " nueva tarea insertada.";			
		} catch (SQLException e) {
			return e.getMessage();
		}
	}
}

