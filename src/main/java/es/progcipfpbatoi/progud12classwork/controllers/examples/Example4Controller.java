package es.progcipfpbatoi.progud12classwork.controllers.examples;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import es.progcipfpbatoi.progud12classwork.services.MySQLConnection;

@Controller
public class Example4Controller {

	@Autowired
	private MySQLConnection mySQLConnection; 
	
	@GetMapping("/example4")
	@ResponseBody
	public String example4InsertWithPreparedStatement() {
		
		StringBuilder respuesta = new StringBuilder();
		
		Connection connection = mySQLConnection.getConnection();
		
		String sql = "INSERT INTO categorias (id,nombre,icono) VALUES (?,?,?)";
        
		try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
			preparedStatement.setInt(1 , 7);
			preparedStatement.setString (2, "Juegos");
			preparedStatement.setString (3, "play.png");
			int affectedRows = preparedStatement.executeUpdate();
			respuesta.append(affectedRows + " nueva categoría insertada");
			
			preparedStatement.setInt(1, 8);
			preparedStatement.setString (2, "Otros");
			preparedStatement.setString (3, "other.png");
			affectedRows = preparedStatement.executeUpdate();
			respuesta.append(affectedRows + " nueva categoría insertada");
            
			return respuesta.toString();
			
        } catch (SQLException e) {
            return e.getMessage();
        }
	}
}
