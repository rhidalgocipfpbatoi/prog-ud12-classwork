package es.progcipfpbatoi.progud12classwork.controllers.actividad1;

import java.sql.DriverManager;
import java.sql.SQLException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class Example1Controller {

	@GetMapping("/example1")
	@ResponseBody
	public String connectionExample() {
		
		try {
	        String dbURL = "jdbc:mysql://localhost/tareas_db";
	        DriverManager.getConnection(dbURL, "rhidalgo-web", "123456789");
	        return "La conexión se ha establecido con éxito";
	     } catch (SQLException e) {
	        return e.getMessage(); 	
	     }
	  }
}
