package es.progcipfpbatoi.progud12classwork.controllers.actividad3;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import es.progcipfpbatoi.progud12classwork.services.MySQLConnection;

@Controller
public class Actividad3Controller {


	@Autowired
	private MySQLConnection mySQLConnection; 
	
	@GetMapping("/tarea-default-add")
	@ResponseBody
	public String insertaTareaPorDefecto(
			@RequestParam String descripcion,
			@RequestParam String usuario) {
		
		String sql = String.format(""
				+ "INSERT INTO tareas "
				+ "(usuario, descripcion, fechaCreacion, "
				+ "vencimiento, prioridad, realizada, categoria_id) "
				+ "VALUES "
				+ "('%s', '%s', '%s', "
				+ "'%s', 'BAJA', 0, NULL)",
				usuario, descripcion, 
				LocalDateTime.now().toString(), 
				LocalDateTime.now().plusDays(2).toString());
		
		Connection connection = mySQLConnection.getConnection();
		
		try (Statement statement = connection.createStatement();) {
			
			int rowsAffected = statement.executeUpdate(sql);
			return rowsAffected + " nueva tarea insertada";
			
		} catch(SQLException ex) {
			return "Ha habido un error:" + ex.getMessage();
		}
		
		
	}

}
